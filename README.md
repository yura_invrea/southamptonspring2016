#  Probabilistic programming practical materials
## Southampton Workshop, Spring 2016 (almost entirely based on MLSS2015 materials)

There is a [getting started guide](docs/getting-started/anglican-getting-started-guide.pdf) which explains how to get up and running with Anglican.

You might find useful a [probabilistic programming reading guide](docs/getting-started/probabilistic-programming-reading.pdf).

After installing everything, there is a [Hello World!](http://viewer.gorilla-repl.org/view.html?source=bitbucket&user=yura_invrea&repo=southamptonspring2016&path=beta-flip/hello-world-worksheet.clj)
worksheet that teaches you enough Clojure and [Anglican](http://www.robots.ox.ac.uk/~fwood/anglican)  to
get started with the included exercises.  

There are three exercises that we recommend you do in the following order:

 1-2 [Gaussian models](http://viewer.gorilla-repl.org/view.html?source=bitbucket&user=yura_invrea&repo=southamptonspring2016&path=gaussian/gaussian-worksheet.clj)

 3   [Financial model](http://viewer.gorilla-repl.org/view.html?source=bitbucket&user=yura_invrea&repo=southamptonspring2016&path=finance/irr.clj)

	
Having finished these you both should be able to program in Anglican 
and have developed reasonable intuitions about how probabilistic programming systems work.

## License

Copyright @ 2016 Southampton March 2016 workshop organisers

This file is part of the Southampton 2016 probabilistic programming practical 
materials (SOTON2016PROBPROG). 

SOTON2016PROBPROG is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SOTON2016PROBPROG is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the [GNU General Public License](gpl-3.0.txt) along 
with SOTON2016PROBPROG.  If not, see 
[http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).