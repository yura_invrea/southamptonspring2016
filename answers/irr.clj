(with-primitive-procedures [irr simulate-price]
  (defquery irr-estimation
    "Model for IRR estimation. Conditioned on past data and horizon length."
    [past-prices past-dividend-indicators horizon]
    (let [; How probable is the divided payout?
          dividend-indicator-dist (flip (sample (uniform-continuous 0 1)))
          ; What is the dividend yield?
          dividend-yield	   	  (sample (uniform-continuous 0.01 0.08))
          ; Condition on past dividend data
          _ (map #(observe dividend-indicator-dist %) past-dividend-indicators)
           
          ; In which years was dividend paid?
          dividend-indicators  (repeatedly horizon
                                           #(sample dividend-indicator-dist))
          ; Generate yields across time
          yields     		   (map #(if % dividend-yield 0.)
                                    dividend-indicators)
          
          ; Create a distribution over step sizes
          step-dist	           (normal 0 1)
          ; Condition on the past price movements
          _ (map #(observe step-dist (apply - %)) (partition 2 1 past-prices))
          random-steps         (repeatedly horizon #(sample step-dist))
          ; Generate price random walk
          prices	 	 	   (simulate-price (last past-prices) random-steps)
          dividends  		   (map * yields prices)
          
          ; Assemble cash flows,
          ;assuming acqusition cost at the start and liquidation at the end
          cash-flows 		   (concat [(- (first prices))] dividends [(last prices)])
          calculated-irr	   (irr cash-flows)]
      ; Predict all values of interest
      (predict :prices prices)
      (predict :yields yields)
      (predict :cash-flows cash-flows)
      (predict :irr calculated-irr))))